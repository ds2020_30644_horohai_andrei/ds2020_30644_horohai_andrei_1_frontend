import axios from 'axios'

const PACIENT_API_BASE_URL = "http://localhost:8080/api/v1/caregivers";


class CaregiverService{

    getCaregivers(){
        return axios.get(PACIENT_API_BASE_URL);
    }

    createCaregiver(caregiver){
        return axios.post(PACIENT_API_BASE_URL, caregiver);
    }

    getCaregiverById(caregiverId){
        return axios.get(PACIENT_API_BASE_URL + '/' + caregiverId);
    }

    updateCaregiver(caregiver, caregiverId){
        return axios.put(PACIENT_API_BASE_URL + '/' + caregiverId, caregiver);
    }

    deleteCaregiver(caregiverId){
        return axios.delete(PACIENT_API_BASE_URL + '/' + caregiverId);
    }

    getCaregiverByName(caregivername)
    {
        let config = {
            params: {
                caregiverName: caregivername 
            },
          }
        return axios.get(PACIENT_API_BASE_URL + '/CaregiverDetails', config)
    }

}

export default new CaregiverService()
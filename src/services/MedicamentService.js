import axios from 'axios'

const PACIENT_API_BASE_URL = "http://localhost:8080/api/v1/medicamente";


class MedicamentService{

    getMedicamente(){
        return axios.get(PACIENT_API_BASE_URL);
    }

    createMedicament(medicament){
        return axios.post(PACIENT_API_BASE_URL, medicament);
    }

    getMedicamentById(medicamentId){
        return axios.get(PACIENT_API_BASE_URL + '/' + medicamentId);
    }

    updateMedicament(medicament, medicamentId){
        return axios.put(PACIENT_API_BASE_URL + '/' + medicamentId, medicament);
    }

    deleteMedicament(medicamentId){
        return axios.delete(PACIENT_API_BASE_URL + '/' + medicamentId);
    }

}

export default new MedicamentService()
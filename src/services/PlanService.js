import axios from 'axios'

const PACIENT_API_BASE_URL = "http://localhost:8080/api/v1/plans";

class PlanService{

  

    createPlan(plan){
        return axios.post(PACIENT_API_BASE_URL, plan);
    }

    getPlanByPacientId(pacientId)
    {
        let config = {
            params: {
                pacientId : pacientId 
            },
          }
        return axios.get(PACIENT_API_BASE_URL + '/PlanById', config)
    }




}

export default new PlanService()
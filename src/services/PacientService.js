import axios from 'axios';

import authHeader from './AuthHeader';


const PACIENT_API_BASE_URL = "http://localhost:8080/api/v1/pacienti";


class PacientService{

    getPacienti(){
        return axios.get(PACIENT_API_BASE_URL);
    }

    createPacient(pacient){
        return axios.post(PACIENT_API_BASE_URL, pacient);
    }

    getPacientById(pacientId){
        return axios.get(PACIENT_API_BASE_URL + '/' + pacientId);
    }

    updatePacient(pacient, pacientId){
        return axios.put(PACIENT_API_BASE_URL + '/' + pacientId, pacient);
    }

    deletePacient(pacientId){
        return axios.delete(PACIENT_API_BASE_URL + '/' + pacientId);
    }


    getPacientByName(pacientname)
    {
        let config = {
            params: {
                pacientName: pacientname 
            },
          }
        return axios.get(PACIENT_API_BASE_URL + '/DetailsByName', config)
    }

    getPacientsByCaregiver(caregivername)
    {
        let config = {
            params: {
                caregiverName: caregivername 
            },
          }
        return axios.get(PACIENT_API_BASE_URL + '/GetPacientsByCaregvier', config)
    }

}

export default new PacientService()
import React from 'react';
import PropTypes from 'prop-types';
import CaregiverService from '../services/CaregiverService';

import SockJS from 'sockjs-client';
import Stomp from 'stompjs';

class CreateCaregiverComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

            id: this.props.match.params.id,
            name: '',
            birth:'',
            gender:'',
            address:'',
            patients:''
        }
        this.changeNameHandler = this.changeNameHandler.bind(this);
        this.changeBirthHandler = this.changeBirthHandler.bind(this);
        this.changeGenderHandler = this.changeGenderHandler.bind(this);
        this.changeAddressHandler = this.changeAddressHandler.bind(this);
        this.changePatientsHandler = this.changePatientsHandler.bind(this);
        this.saveOrUpdateCaregiver  = this.saveOrUpdateCaregiver.bind(this);

    
    }


    componentDidMount(){


        if(this.state.id == -1){
            return
        }else{
                    CaregiverService.getCaregiverById(this.state.id).then((res1) => {
                        let caregiver = res1.data;
                        this.setState({name: caregiver.name,
                                        birth : caregiver.birth,
                                        gender : caregiver.gender,
                                        address : caregiver.address,
                                        patients : caregiver.patients
                                    });
                    });
            }
    }



    saveOrUpdateCaregiver = (c) => {
        c.preventDefault();

        let caregiver = {name: this.state.name, birth : this.state.birth , gender: this.state.gender, address : this.state.address, patients : this.state.patients};
        console.log('caregiver= > ' + JSON.stringify(caregiver));
        if(this.state.id == -1){

            CaregiverService.createCaregiver(caregiver).then(res1 =>{
                this.props.history.push('/admin');
            });

        }else{

            CaregiverService.updateCaregiver(caregiver, this.state.id).then(res1 => {
                this.props.history.push('/admin'); 
            });
       
        }
    }




    changeNameHandler = (event) =>{
        this.setState({name:event.target.value});
    }
    changeBirthHandler = (event) =>{
        this.setState({birth :event.target.value});
    }
    changeGenderHandler = (event) =>{
        this.setState({gender :event.target.value});
    }
    changeAddressHandler = (event) =>{
        this.setState({address :event.target.value});
    }
    changePatientsHandler = (event) =>{
        this.setState({patients :event.target.value});
    }
    
    cancel(){
        this.props.history.push('/admin'); 
    }
    
    getTitle(){
        if(this.state.id  == -1){
            return <h3 className = "text-center">Add Caregiver</h3>
        }else{
            return <h3 className="text-center">Update Caregiver</h3>
        }
    }
    

    render() {
        return (
            <div>
                <br></br>
                <div className="container">
                    <div className="row">
                        <div className="card col-md-6 offset-md-3 offset-md-3">
                            {
                                this.getTitle()
                            }
                            <div className="card-body">
                                <form>
                                    <div className="form-group">
                                        <label>Name :</label>
                                        <input placeholder="Name" name="name" className="form-control" value={this.state.name} onChange={this.changeNameHandler}/>
                                    </div>
                                    <div className="form-group">
                                        <label>Birth Date :</label>
                                        <input placeholder="Birth Date" name="birth" className="form-control" value={this.state.birth} onChange={this.changeBirthHandler}/>
                                    </div>
                                    <div className="form-group">
                                        <label>Gender :</label>
                                        <input placeholder="Gender" name="gender" className="form-control" value={this.state.gender} onChange={this.changeGenderHandler}/>
                                    </div>
                                    <div className="form-group">
                                        <label>Address :</label>
                                        <input placeholder="Address" name="address" className="form-control" value={this.state.address} onChange={this.changeAddressHandler}/>
                                    </div>
                                    {/*
                                        <div className="form-group">
                                        <label>Patients taken care of:</label>
                                        <input placeholder="Patients taken care of" name="patients" className="form-control" value={this.state.patients} onChange={this.changePatientsHandler}/>
                                    </div>
                                    */}
                                    <button className="btn btn-success" onClick={this.saveOrUpdateCaregiver}>Save</button>
                                    <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{marginLeft: "10px"}}>Cancel</button>
                                </form>
                            </div>

                        </div>

                    </div>

                </div>
                
            </div>
        );
    }
}

CreateCaregiverComponent.propTypes = {};

export default CreateCaregiverComponent;

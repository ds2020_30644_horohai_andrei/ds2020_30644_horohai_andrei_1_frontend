import React, {Component} from 'react'
import PropTypes from 'prop-types'
import PacientService from '../services/PacientService'
import CaregiverService from '../services/CaregiverService'
import MedicamentService from '../services/MedicamentService'


class DoctorComponent extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            pacienti : [] ,
            caregivers : [] ,
            medicamente : []
        }
        this.addPacient = this.addPacient.bind(this);
        this.editPacient = this.editPacient.bind(this);
        this.deletePacient = this.deletePacient.bind(this);

        this.addCaregiver = this.addCaregiver.bind(this);
        this.editCaregiver = this.editCaregiver.bind(this);
        this.deleteCaregiver = this.deleteCaregiver.bind(this);

        this.addMedicament = this.addMedicament.bind(this);
        this.editMedicament = this.editMedicament.bind(this);
        this.deleteMedicament = this.deleteMedicament.bind(this);
    }


    componentDidMount(){
        PacientService.getPacienti().then((res) =>{
            this.setState({ pacienti: res.data });

        });

        CaregiverService.getCaregivers().then((res) =>{
            this.setState({ caregivers: res.data });

        });

        MedicamentService.getMedicamente().then((res) =>{
            this.setState({ medicamente: res.data });

        });
    }


    //pt medicamente

    
    deleteMedicament(id){
       
        MedicamentService.deleteMedicament(id).then( res => {
            this.setState({medicamente : this.state.medicamente.filter(medicament => medicament.id !== id )});
        });
    }
    
    editMedicament(id){

        this.props.history.push(`/add-medicament/${id}`);
    }
    
    
   

    addMedicament(){
        this.props.history.push('/add-medicament/-1');
    }




    //pt caregiver

    deleteCaregiver(id){
       
        CaregiverService.deleteCaregiver(id).then( res => {
            this.setState({caregivers : this.state.caregivers.filter(caregiver => caregiver.id !== id )});
        });
    }
    
    editCaregiver(id){

        this.props.history.push(`/add-caregiver/${id}`);
    }
    
    
   

    addCaregiver(){
        this.props.history.push('/add-caregiver/-1');
    }

    






    //pt pactienti


    createPlan(id){
        this.props.history.push(`/create-plan/${id}`);
    }

    deletePacient(id){
       
        PacientService.deletePacient(id).then( res => {
            this.setState({pacienti : this.state.pacienti.filter(pacient => pacient.id !== id )});
        });
    }
    
    editPacient(id){

        this.props.history.push(`/add-pacient/${id}`);
    }
    
    

    addPacient(){
        this.props.history.push('/add-pacient/-1');
    }

  


    render() {
        return (
            <div>
                <div>
                    <h2 className="text-center">Lista Pacienti</h2>
                    <div className="row">
                        <button className="btn btn-primary" onClick={this.addPacient}>Add Pacient</button>
                    </div>
                    <div className="row">
                        <table className = "table table-striped tablebordered">
                            
                                <thead>
                                    <tr>
                                        <th>  Name</th>
                                        <th>  Birth Date</th>
                                        <th>  Gender</th>
                                        <th>  Address</th>
                                        <th>  Medical Record</th>
                                        <th> Actions </th>

                                    </tr>
                                </thead>
                                
                                <tbody>

                                    {
                                        this.state.pacienti.map(
                                            pacient => 
                                            <tr key = {pacient.id}>
                                                <td> {pacient.name}</td>
                                                <td> {pacient.birth}</td>
                                                <td> {pacient.gender}</td>
                                                <td> {pacient.address}</td>
                                                <td> {pacient.record}</td>
                                                <td>
                                                    <button onClick = {()=>this.editPacient(pacient.id)} className ="btn btn-info">Update</button>
                                                    <button style = {{marginLeft: "10px"}} onClick = {()=>this.deletePacient(pacient.id)} className ="btn btn-danger">Delete</button>
                                                    <button style = {{marginLeft: "10px"}} onClick = {()=>this.createPlan(pacient.id)} className ="btn btn-success">Create Plan</button>
                                                    
                                                </td>

                                            </tr>
                                        )
                                    }    


                                </tbody>

                        </table>
                    </div>
                </div>
                <div>
                    <h2 className="text-center">Lista Caregivers</h2>
                    <div className="row">
                        <button className="btn btn-primary" onClick={this.addCaregiver}>Add Caregiver</button>
                    </div>
                    <div className="row">
                        <table className = "table table-striped tablebordered">
                            
                                <thead>
                                    <tr>
                                        <th>  Name</th>
                                        <th>  Birth Date</th>
                                        <th>  Gender</th>
                                        <th>  Address</th>
                                     {/*   <th>  Patients</th> */}
                                        <th> Actions </th>

                                    </tr>
                                </thead>
                                
                                <tbody>

                                    {
                                        this.state.caregivers.map(
                                            caregiver => 
                                            <tr key = {caregiver.id}>
                                                <td> {caregiver.name}</td>
                                                <td> {caregiver.birth}</td>
                                                <td> {caregiver.gender}</td>
                                                <td> {caregiver.address}</td>
                                            {/*    <td> {caregiver.patients}</td> */}
                                                <td>
                                                    <button onClick = {()=>this.editCaregiver(caregiver.id)} className ="btn btn-info">Update</button>
                                                    <button style = {{marginLeft: "10px"}} onClick = {()=>this.deleteCaregiver(caregiver.id)} className ="btn btn-danger">Delete</button>
                                                    
                                                </td>

                                            </tr>
                                        )
                                    }    


                                </tbody>

                        </table>
                    </div>
                </div>
                <div>
                    <h2 className="text-center">Lista Medicamente</h2>
                    <div className="row">
                        <button className="btn btn-primary" onClick={this.addMedicament}>Add Medicament</button>
                    </div>
                    <div className="row">
                        <table className = "table table-striped tablebordered">
                            
                                <thead>
                                    <tr>
                                        <th>  Name</th>
                                        <th>  Side Effects</th>
                                        <th>  Dosage</th>
                                    
                                    </tr>
                                </thead>
                                
                                <tbody>

                                    {
                                        this.state.medicamente.map(
                                            medicament => 
                                            <tr key = {medicament.id}>
                                                <td> {medicament.name}</td>
                                                <td> {medicament.side}</td>
                                                <td> {medicament.dosage}</td>
                                                <td>

                                                    <button onClick = {()=>this.editMedicament(medicament.id)} className ="btn btn-info">Update</button>
                                                    <button style = {{marginLeft: "10px"}} onClick = {()=>this.deleteMedicament(medicament.id)} className ="btn btn-danger">Delete</button>
                                                    
                                                </td>

                                            </tr>
                                        )
                                    }    


                                </tbody>

                        </table>
                    </div>
                </div>
            </div>
        );
    }
}

DoctorComponent.propTypes = {};

export default DoctorComponent;

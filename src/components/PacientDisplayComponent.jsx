import React, { Component } from 'react';
import PacientDisplayService from "../services/PacientDisplayService"
import PacientService from '../services/PacientService'
import PlanService from '../services/PlanService';
import AuthService from '../services/AuthService';




class PacientDisplayComponent extends Component {

    constructor(props){
        super(props)
        this.state={
            name: this.props.match.params.name,
            birth: '',
            gender: '',
            address: '',
            record: '',
            plans:[]
        }
    }

    componentDidMount(){
        let pacient = {name: this.state.name, birth : ' ' , gender: ' ', address : ' ', record : ' '};

        PacientService.getPacientByName(pacient.name).then((res) =>{
            pacient = res.data
            this.setState({ id: pacient.id,
                            birth : pacient.birth,
                            gender: pacient.gender,
                            address : pacient.address,
                            record : pacient.record
                        });
             PlanService.getPlanByPacientId(pacient.id).then((res)=>{
                 this.setState({plans: res.data})
             });
        });

     
    }














    render() {
        return (
            <div>
            <br></br>
                <div className="container">
                <div className="row">
                    <div className="card col-md-6 offset-md-3 offset-md-3">
                        <h2>Pacient Page</h2>
                        <div className="card-body">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th scope="row">Name :</th>
                                        <td>{this.state.name}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Birth Date :</th>
                                        <td>{this.state.birth}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Gender : </th>
                                        <td>{this.state.gender}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Address : </th>
                                        <td>{this.state.address}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Record : </th>
                                        <td>{this.state.record}</td>
                                    </tr>
                                </tbody>
                            </table>
                            <div className="row">
                                <br></br>
                                <h3>Medication Plan</h3>
                                <br></br>
                                <table className = "table table-striped mx-auto" >
                                    
                                        <thead>
                                            <tr>
                                                <th>  Medicine</th>
                                                <th>  Dosage </th>
                                                <th>  Period</th>
                                            
                                            </tr>
                                        </thead>
                                        
                                        <tbody>

                                            {
                                                this.state.plans.map(
                                                    plan => 
                                                    <tr key = {plan.id}>
                                                        <td> {plan.medicament}</td>
                                                        <td> {plan.dosage}</td>
                                                        <td> {plan.period}</td>
                                                    

                                                    </tr>
                                                )
                                            }    


                                        </tbody>

                                </table>
                            
                        

                            </div>
                        </div>

                    </div>

                </div>




                </div>
                
            </div>
        );
    }
}

export default PacientDisplayComponent;
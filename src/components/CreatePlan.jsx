import React, { Component } from 'react';
import PlanService from '../services/PlanService'

class CreatePlan extends Component {

    constructor(props){
        super(props);
        this.state = {
            pacientId : this.props.match.params.id,
            medicament : '',
            dosage : '',
            period : '',

        }



        this.changeMedicamentHandler = this.changeMedicamentHandler.bind(this);
        this.changeDosageHandler = this.changeDosageHandler.bind(this);
        this.changePeriodHandler = this.changePeriodHandler.bind(this);
        this.savePlan = this.savePlan.bind(this);
    }

    componentDidMount(){

    }

    savePlan = (p) =>{
        p.preventDefault();
        let plan = {pacientId : this.state.pacientId, medicament : this.state.medicament, dosage: this.state.dosage, period : this.state.period};
        console.log('plan => ' + JSON.stringify(plan));
        
        PlanService.createPlan(plan).then(res =>{
            this.props.history.push('/admin');    
        });
    }
    
    changeMedicamentHandler = (event) =>{
        this.setState({medicament : event.target.value});
    }

    changeDosageHandler = (event) =>{
        this.setState({dosage : event.target.value});
    }
    changePeriodHandler= (event) =>{
        this.setState({period : event.target.value});
    }

    cancel(){
        this.props.history.push('/admin'); 
    }



    render() {
        return (
            <div>
            <br></br>
            <div className="container">
                <div className="row">
                    <div className="card col-md-6 offset-md-3 offset-md-3">
                        <h2>Create Plan</h2>
                        <div className="card-body">
                            <form>
                                <div className="form-group">
                                    <label>Medicament :</label>
                                    <input placeholder="Medicament" name="medicament" className="form-control" value={this.state.medicament} onChange={this.changeMedicamentHandler}/>
                                </div>
                                <div className="form-group">
                                    <label>Dosage :</label>
                                    <input placeholder="Dosage" name="dosage" className="form-control" value={this.state.dosage} onChange={this.changeDosageHandler}/>
                                </div>
                                <div className="form-group">
                                    <label>Period :</label>
                                    <input placeholder="Period" name="period" className="form-control" value={this.state.period} onChange={this.changePeriodHandler}/>
                                </div>
                                <button className="btn btn-success" onClick={this.savePlan}>Save</button>
                                <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{marginLeft: "10px"}}>Cancel</button>
                            </form>
                        </div>

                    </div>

                </div>

            </div>
            
        </div>
        );
    }
}

export default CreatePlan;
import React from 'react';
import PropTypes from 'prop-types';

class HeaderComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div>
                <header>
                    <nav className="navbar navbar-extend.-md navbar-dark bg-dark">
                        <div><a href="http://localhost:3000/" className="navbar-brand">Medical System</a></div>

                    </nav>
                </header>
            </div>
        );
    }
}

HeaderComponent.propTypes = {};

export default HeaderComponent;

import React, { Component } from 'react';
import CaregiverService from '../services/CaregiverService';
import PacientService from '../services/PacientService';
import AuthService from '../services/AuthService';
import SockJS from 'sockjs-client';
import Stomp from 'stompjs';


class CaregiverDisplayComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {

            name : this.props.match.params.name,
            id:'',
            birth:'',
            gender: '',
            address: '', 
            pacienti : []
         
        }

    

       
 
    }
    

    
    componentDidMount(){
      
        let caregiver = { name : this.state.name, id:'', birth:'', gender: '', address: '' }
       
        CaregiverService.getCaregiverByName( caregiver.name).then((res) => {
            caregiver = res.data;
            this.setState({  id : caregiver.id,
                             birth: caregiver.birth,
                             gender: caregiver.gender,
                             address : caregiver.address



                         });
                         

                
                
        });

        PacientService.getPacientsByCaregiver(caregiver.name).then((res=>{
            this.setState({pacienti : res.data})
        }));




        


      
    }


    
    getPlan(id){
        this.props.history.push(`/get-plan/${id}`);
    }

 






    render() {
        return (
            <div>
                <br></br>
                <div className="container">
                <div className="row">
                    <div className="card col-md-6 offset-md-3 offset-md-3">
                        <h2>Caregiver Page</h2>
                        <div className="card-body">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th scope="row">Name :</th>
                                        <td>{this.state.name}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Birth Date :</th>
                                        <td>{this.state.birth}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Gender : </th>
                                        <td>{this.state.gender}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Address : </th>
                                        <td>{this.state.address}</td>
                                    </tr>
                               
                                </tbody>
                            </table>
                        </div>
                    </div>    
                            <div className="row mx-auto">
                                <table className = "table table-striped  mx-auto ">
                                    
                                        <thead>
                                            <tr>
                                                <th>  Name</th>
                                                <th>  Birth Date</th>
                                                <th>  Gender</th>
                                                <th>  Address</th>
                                                <th>  Medical Record</th>

                                            </tr>
                                        </thead>
                                        
                                        <tbody>

                                            {
                                                this.state.pacienti.map(
                                                    pacient => 
                                                    <tr key = {pacient.id}>
                                                        <td> {pacient.name}</td>
                                                        <td> {pacient.birth}</td>
                                                        <td> {pacient.gender}</td>
                                                        <td> {pacient.address}</td>
                                                        <td> {pacient.record}</td>
                                                        <td>
                                                        <button style = {{marginLeft: "10px"}} onClick = {()=>this.getPlan(pacient.id)} className ="btn btn-success">Get Plan</button>
                                                        </td>
                                                    </tr>
                                                )
                                            }    


                                        </tbody>

                                </table>
                            
                        

                    </div>

                </div>




                </div>
                
            </div>
        );
    }
}

export default CaregiverDisplayComponent;
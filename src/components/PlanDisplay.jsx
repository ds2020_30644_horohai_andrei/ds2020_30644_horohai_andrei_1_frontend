import React, { Component } from 'react';
import PlanService from '../services/PlanService';

class PlanDisplay extends React.Component {

    constructor(props) {
        super(props);
        this.state = {

            id: this.props.match.params.id,
            plans: []
        }



      
    }

    componentDidMount(){

        PlanService.getPlanByPacientId(this.state.id).then((res) => {

                this.setState({plans: res.data});
        });
    }



    render() {
        return (
            <div>
                 <div>
                    <h2 className="text-center">Plan Medical</h2>
                        <div className="row">
                        <table className = "table table-striped tablebordered">
                            
                                <thead>
                                    <tr>
                                        <th>  Medicine  </th>
                                        <th>  Dosage  </th>
                                        <th>  Period</th>
                                    

                                    </tr>
                                </thead>
                                
                                <tbody>

                                    {
                                        this.state.plans.map(
                                            plan => 
                                            <tr key = {plan.id}>
                                                <td> {plan.medicament}</td>
                                                <td> {plan.dosage}</td>
                                                <td> {plan.period}</td>
                                                
                                            </tr>
                                        )
                                    }    


                                </tbody>

                        </table>
                    </div>
                </div>
                
            </div>
        );
    }
}

export default PlanDisplay;
import React from 'react';
import PropTypes from 'prop-types';
import MedicamentService from '../services/MedicamentService';

class CreateMedicamentComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

            id: this.props.match.params.id,
            name: '',
            side:'',
            dosage:''
        }


        this.changeNameHandler = this.changeNameHandler.bind(this);
        this.changeSideHandler = this.changeSideHandler.bind(this);
        this.changeDosageHandler = this.changeDosageHandler.bind(this);
      
    }


    componentDidMount(){


        if(this.state.id == -1){
            return
        }else{
                    MedicamentService.getMedicamentById(this.state.id).then((res) => {
                        let medicament = res.data;
                        this.setState({
                                        name: medicament.name,
                                        side : medicament.side,
                                        dosage : medicament.dosage

                                    });
                    });
            }
    }



    saveOrUpdateMedicament = (m) => {
        m.preventDefault();

        let medicament = {name: this.state.name, side : this.state.side , dosage: this.state.dosage};
        console.log('medicament= > ' + JSON.stringify(medicament));
        if(this.state.id == -1){

            MedicamentService.createMedicament(medicament).then(res =>{
                this.props.history.push('/admin');
            });

        }else{

            MedicamentService.updateMedicament(medicament, this.state.id).then(res => {
                this.props.history.push('/admin'); //dupa ce am trimis vreau sa ma duca la pagina /pacienti
            });
       
        }
    }




    changeNameHandler = (event) =>{
        this.setState({name:event.target.value});
    }
    changeSideHandler = (event) =>{
        this.setState({side :event.target.value});
    }
    changeDosageHandler = (event) =>{
        this.setState({dosage :event.target.value});
    }
   

    
    cancel(){
        this.props.history.push('/admin'); 
    }
    

    getTitle(){
        if(this.state.id  == -1){
            return <h3 className = "text-center">Add Medicament</h3>
        }else{
            return <h3 className="text-center">Update Medicament</h3>
        }
    }
    

    render() {
        return (
            <div>
                <br></br>
                <div className="container">
                    <div className="row">
                        <div className="card col-md-6 offset-md-3 offset-md-3">
                            {
                                this.getTitle()
                            }
                            <div className="card-body">
                                <form>
                                    <div className="form-group">
                                        <label>Name :</label>
                                        <input placeholder="Name" name="name" className="form-control" value={this.state.name} onChange={this.changeNameHandler}/>
                                    </div>
                                    <div className="form-group">
                                        <label>Side Effects :</label>
                                        <input placeholder="Side Effects" name="side" className="form-control" value={this.state.side} onChange={this.changeSideHandler}/>
                                    </div>
                                    <div className="form-group">
                                        <label>Dosage :</label>
                                        <input placeholder="Dosage" name="dosage" className="form-control" value={this.state.dosage} onChange={this.changeDosageHandler}/>
                                    </div>
                                    <button className="btn btn-success" onClick={this.saveOrUpdateMedicament}>Save</button>
                                    <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{marginLeft: "10px"}}>Cancel</button>
                                </form>
                            </div>

                        </div>

                    </div>

                </div>
                
            </div>
        );
    }
}

CreateMedicamentComponent.propTypes = {};

export default CreateMedicamentComponent;

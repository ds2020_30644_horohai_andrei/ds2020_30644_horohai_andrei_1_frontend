import React from 'react';
import PropTypes from 'prop-types';
import PacientService from '../services/PacientService';


import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

class CreatePacientComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

            id: this.props.match.params.id,
            name: '',
            birth:'',
            gender:'',
            address:'',
            record:'',
            caregiverName:''
        }
        this.changeNameHandler = this.changeNameHandler.bind(this);
        this.changeBirthHandler = this.changeBirthHandler.bind(this);
        this.changeGenderHandler = this.changeGenderHandler.bind(this);
        this.changeAddressHandler = this.changeAddressHandler.bind(this);
        this.changeRecordHandler = this.changeRecordHandler.bind(this);
        this.changeCaregiverHandler = this.changeCaregiverHandler.bind(this);
        this.saveOrUpdatePacient  = this.saveOrUpdatePacient.bind(this);
    }


    componentDidMount(){


        if(this.state.id == -1){
            return
        }else{
                    PacientService.getPacientById(this.state.id).then((res) => {
                        let pacient = res.data;
                        this.setState({name: pacient.name,
                                        birth : pacient.birth,
                                        gender : pacient.gender,
                                        address : pacient.address,
                                        record : pacient.record,
                                        caregiverName : pacient.caregiverName
                                    });
                    });
            }
    }


  

    saveOrUpdatePacient = (e) => {
        e.preventDefault();

        let pacient = {name: this.state.name, birth : this.state.birth , gender: this.state.gender, address : this.state.address, record : this.state.record , caregiverName : this.state.caregiverName};
        console.log('pacient= > ' + JSON.stringify(pacient));
        if(this.state.id == -1){

            PacientService.createPacient(pacient).then(res =>{
                if(res.status == 200){console.log("succes")};
                this.props.history.push('/admin');
              
            });

        }else{

            PacientService.updatePacient(pacient, this.state.id).then(res => {
                this.props.history.push('/admin'); 
            });
       
        }
    }




    changeNameHandler = (event) =>{
        this.setState({name:event.target.value});
    }
    changeBirthHandler = (event) =>{
        this.setState({birth :event.target.value});
    }
    changeGenderHandler = (event) =>{
        this.setState({gender :event.target.value});
    }
    changeAddressHandler = (event) =>{
        this.setState({address :event.target.value});
    }
    changeRecordHandler = (event) =>{
        this.setState({record :event.target.value});
    }
    changeCaregiverHandler = (event) =>{
        this.setState({caregiverName :event.target.value});
    }
    
    cancel(){
        this.props.history.push('/admin'); 
    }
    
    getTitle(){
        if(this.state.id  == -1){
            return <h3 className = "text-center">Add Pacient</h3>
        }else{
            return <h3 className="text-center">Update Pacient</h3>
        }
    }
    

    render() {
        return (
            <div>
                <br></br>
                <div className="container">
                    <div className="row">
                        <div className="card col-md-6 offset-md-3 offset-md-3">
                            {
                                this.getTitle()
                            }
                            <div className="card-body">
                                <form>
                                    <div className="form-group">
                                        <label>Name :</label>
                                        <input placeholder="Name" name="name" className="form-control" value={this.state.name} onChange={this.changeNameHandler}/>
                                    </div>
                                    <div className="form-group">
                                        <label>Birth Date :</label>
                                        <input placeholder="Birth Date" name="birth" className="form-control" value={this.state.birth} onChange={this.changeBirthHandler}/>
                                    </div>
                                    <div className="form-group">
                                        <label>Gender :</label>
                                        <input placeholder="Gender" name="gender" className="form-control" value={this.state.gender} onChange={this.changeGenderHandler}/>
                                    </div>
                                    <div className="form-group">
                                        <label>Address :</label>
                                        <input placeholder="Address" name="address" className="form-control" value={this.state.address} onChange={this.changeAddressHandler}/>
                                    </div>
                                    <div className="form-group">
                                        <label>Medical Record :</label>
                                        <input placeholder="Medical Record" name="record" className="form-control" value={this.state.record} onChange={this.changeRecordHandler}/>
                                    </div>
                                    <div className="form-group">
                                        <label>Caregiver Name :</label>
                                        <input placeholder="Caregiver Name" name="caregiverName" className="form-control" value={this.state.caregiverName} onChange={this.changeCaregiverHandler}/>
                                    </div>
                                    <button className="btn btn-success" onClick={this.saveOrUpdatePacient}>Save</button>
                                    <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{marginLeft: "10px"}}>Cancel</button>
                                </form>
                            </div>

                        </div>

                    </div>

                </div>
                
            </div>
        );
    }
}

CreatePacientComponent.propTypes = {};

export default CreatePacientComponent;

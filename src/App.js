import logo from './logo.svg';
import './App.css';
import React, { Component } from "react";
import {BrowserRouter as Router , Redirect, Route, Switch, Link }from 'react-router-dom'
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import DoctorComponent from './components/DoctorComponent';
import HeaderComponent from './components/HeaderComponent';
import CreatePacientComponent from './components/CreatePacientComponent';
import CreateCaregiverComponent from './components/CreateCaregiverComponent';
import CreateMedicamentComponent from './components/CreateMedicamentComponent';
import CreatePlan from './components/CreatePlan';
import LoginComponent from './components/LoginComponent';
import AuthService from './services/AuthService';
import PacientDisplayComponent from './components/PacientDisplayComponent';
import CaregiverDisplayComponent from './components/CaregiverDisplayComponent';
import PlanDisplay from './components/PlanDisplay';
import NotificationComponent from './components/NotificationComponent';








class App extends Component {
  constructor(props) {
    super(props);
    this.logOut = this.logOut.bind(this);

  }

 

  logOut() {
    AuthService.logout();
  }


  render(){ 
  
    return (

      <div>
        <Router>
        
      <HeaderComponent></HeaderComponent>
        <div className="container mt-3">
          <Switch>
            <Route exact path={["/", "/home"]} component={LoginComponent} />
            <Route exact path="/login" component={LoginComponent} />
            <Route path="/user/:name" component={PacientDisplayComponent} />
            <Route path="/mod/:name" component={CaregiverDisplayComponent} />
            <Route path="/admin" component={DoctorComponent} />
            <Route path="/notif" component={NotificationComponent} />
            <Route path="/caregiverNotification" component={NotificationComponent} />
            <Route path="/add-pacient/:id" component={CreatePacientComponent}></Route>
            <Route path="/add-caregiver/:id" component={CreateCaregiverComponent}></Route>
            <Route path="/add-medicament/:id" component={CreateMedicamentComponent}></Route>
            <Route path="/create-plan/:id" component={CreatePlan}></Route>
            <Route path="/get-plan/:id" component={PlanDisplay}></Route>
          </Switch>
        </div>
        </Router>
      
      </div>
    );
  }
  
}

export default App;